---
slug: /
id: intro
sidebar_position: 1
---

# Overview 

The OpenAIRE Graph is one of the largest open scholarly record collections worldwide, key in fostering Open Science and establishing its practices in the daily research activities.
Conceived as a public and transparent good, populated out of data sources trusted by scientists, the Graph aims at bringing discovery, monitoring, and assessment of science back in the hands of the scientific community.

Imagine a vast collection of research products all linked together, contextualised and openly available. For the past years OpenAIRE has been working to gather this valuable record. It is a massive collection of metadata and links between scientific products such as articles, datasets, software, and other research products, entities like organisations, funders, funding streams, projects, communities, and data sources.

As of today, the OpenAIRE Graph aggregates hundreds of millions of metadata records (and links among them)  from multiple data sources trusted by scientists, including:

* Repositories registered in OpenDOAR or re3data.org (soon FAIRSharing.org)
* Open Access journals registered in DOAJ
* Crossref
* Unpaywall
* ORCID
* Microsoft Academic Graph
* Datacite

After cleaning, deduplication, enrichment and full-text mining processes, the graph is analysed to produce statistics for the [OpenAIRE MONITOR](https://monitor.openaire.eu), the [Open Science Observatory](https://osobservatory.openaire.eu), made discoverable via the [OpenAIRE EXPLORE](https://explore.openaire.eu) and programmatically accessible via [OpenAIRE Public APIs](https://develop.openaire.eu).
Last but not least, the Graph data are openly available and can be used by third-parties to create added value services. 
